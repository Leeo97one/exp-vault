package yolocorp.xyz.expvault;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.List;

public class ExpEconomy implements Economy {
	private final ExpVault expVault;

	public ExpEconomy(ExpVault expVault) {
		this.expVault = expVault;
	}

	@Override
	public boolean isEnabled() {
		return expVault.isEnabled();
	}

	@Override
	public String getName() {
		return expVault.getName();
	}

	@Override
	public boolean hasBankSupport() {
		return false;
	}

	@Override
	public int fractionalDigits() {
		return 0;
	}

	@Override
	public String format(double amount) {
		return amount + " exp";
	}

	@Override
	public String currencyNamePlural() {
		return currencyNameSingular();
	}

	@Override
	public String currencyNameSingular() {
		return "exp";
	}

	@Override
	public boolean hasAccount(String playerName) {
		return hasAccount(getPlayer(playerName));
	}

	@Override
	public boolean hasAccount(OfflinePlayer player) {
		return player != null;
	}

	@Override
	public boolean hasAccount(String playerName, String worldName) {
		return hasAccount(playerName);
	}

	@Override
	public boolean hasAccount(OfflinePlayer player, String worldName) {
		return hasAccount(player);
	}

	@Override
	public double getBalance(String playerName) {
		Player player = getPlayer(playerName);
		if (player == null) {
			return 0;
		}

		return getBalance(player);
	}

	@Override
	public double getBalance(OfflinePlayer player) {
		return ExpUtil.getTotalExperience(getPlayer(player));
	}

	@Override
	public double getBalance(String playerName, String world) {
		return getBalance(playerName);
	}

	@Override
	public double getBalance(OfflinePlayer player, String world) {
		return getBalance(player);
	}

	@Override
	public boolean has(String playerName, double amount) {
		return has(getPlayer(playerName), amount);
	}

	@Override
	public boolean has(OfflinePlayer player, double amount) {
		return getBalance(player) >= amount;
	}

	@Override
	public boolean has(String playerName, String worldName, double amount) {
		return has(playerName, amount);
	}

	@Override
	public boolean has(OfflinePlayer player, String worldName, double amount) {
		return has(player, amount);
	}

	@Override
	public EconomyResponse withdrawPlayer(String playerName, double amount) {
		return withdrawPlayer(getPlayer(playerName), amount);
	}

	@Override
	public EconomyResponse withdrawPlayer(OfflinePlayer player, double amount) {
		ExpUtil.setTotalExperience(
				getPlayer(player),
				(int) (getBalance(player) - amount)
		);

		return new EconomyResponse(amount, getBalance(player), EconomyResponse.ResponseType.SUCCESS, null);
	}

	@Override
	public EconomyResponse withdrawPlayer(String playerName, String worldName, double amount) {
		return withdrawPlayer(playerName, amount);
	}

	@Override
	public EconomyResponse withdrawPlayer(OfflinePlayer player, String worldName, double amount) {
		return withdrawPlayer(player, amount);
	}

	@Override
	public EconomyResponse depositPlayer(String playerName, double amount) {
		return depositPlayer(getPlayer(playerName), amount);
	}

	@Override
	public EconomyResponse depositPlayer(OfflinePlayer player, double amount) {
		ExpUtil.setTotalExperience(
				getPlayer(player),
				(int) (getBalance(player) + amount)
		);

		return new EconomyResponse(amount, getBalance(player), EconomyResponse.ResponseType.SUCCESS, null);
	}

	@Override
	public EconomyResponse depositPlayer(String playerName, String worldName, double amount) {
		return depositPlayer(playerName, amount);
	}

	@Override
	public EconomyResponse depositPlayer(OfflinePlayer player, String worldName, double amount) {
		return depositPlayer(player, amount);
	}

	@Override
	public EconomyResponse createBank(String name, String player) {
		return bankBalance(name);
	}

	@Override
	public EconomyResponse createBank(String name, OfflinePlayer player) {
		return bankBalance(name);
	}

	@Override
	public EconomyResponse deleteBank(String name) {
		return bankBalance(name);
	}

	@Override
	public EconomyResponse bankBalance(String name) {
		return new EconomyResponse(0, 0, EconomyResponse.ResponseType.NOT_IMPLEMENTED, expVault.getName() + " does not support bank.");
	}

	@Override
	public EconomyResponse bankHas(String name, double amount) {
		return bankBalance(name);
	}

	@Override
	public EconomyResponse bankWithdraw(String name, double amount) {
		return bankBalance(name);
	}

	@Override
	public EconomyResponse bankDeposit(String name, double amount) {
		return bankBalance(name);
	}

	@Override
	public EconomyResponse isBankOwner(String name, String playerName) {
		return bankBalance(name);
	}

	@Override
	public EconomyResponse isBankOwner(String name, OfflinePlayer player) {
		return bankBalance(name);
	}

	@Override
	public EconomyResponse isBankMember(String name, String playerName) {
		return bankBalance(name);
	}

	@Override
	public EconomyResponse isBankMember(String name, OfflinePlayer player) {
		return bankBalance(name);
	}

	@Override
	public List<String> getBanks() {
		return null;
	}

	@Override
	public boolean createPlayerAccount(String playerName) {
		return createPlayerAccount(getPlayer(playerName));
	}

	@Override
	public boolean createPlayerAccount(OfflinePlayer player) {
		return player != null;
	}

	@Override
	public boolean createPlayerAccount(String playerName, String worldName) {
		return createPlayerAccount(playerName);
	}

	@Override
	public boolean createPlayerAccount(OfflinePlayer player, String worldName) {
		return createPlayerAccount(player);
	}

	private Player getPlayer(String playerName) {
		return expVault.getServer().getPlayerExact(playerName);
	}

	private Player getPlayer(OfflinePlayer player) {
		return expVault.getServer().getPlayer(player.getUniqueId());
	}
}
