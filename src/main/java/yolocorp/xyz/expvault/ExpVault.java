package yolocorp.xyz.expvault;

import net.milkbowl.vault.economy.Economy;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.java.JavaPlugin;

public final class ExpVault extends JavaPlugin {

	@Override
	public void onEnable() {
		// Plugin startup logic
		getLogger().info("Bonjour.");
		// Vault
		getServer().getServicesManager().register(Economy.class, new ExpEconomy(this), this, ServicePriority.Normal);
	}

	@Override
	public void onDisable() {
		// Plugin shutdown logic
		getLogger().info("Au revoir.");
	}
}
