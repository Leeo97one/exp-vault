# ExpVault

A very simple Bukkit plugin which implements the [Vault](https://github.com/MilkBowl/Vault) Economy service to use the player's [experience](https://minecraft.fandom.com/wiki/Experience) as a currency.
No config, it should just work with any plugin compatible with Vault.